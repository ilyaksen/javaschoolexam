package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        String infix = infixToPostfix(statement);
        if (infix == null)
            return null;

        Deque<Double> operandSack = new ArrayDeque<>();
        String[] elements = infix.split(" ");

        //evaluate of postfix expression
        try {
            for (String element : elements) {
                try {
                    Double number = Double.parseDouble(element);
                    operandSack.push(number);
                } catch (NumberFormatException e) {
                    Double operand2 = operandSack.pop();
                    Double operand1 = operandSack.pop();
                    operandSack.push(calculate(element, operand1, operand2));
                }
            }
        } catch (Exception e) {
            return null;
        }

        double result = operandSack.pop();

        if (result == Double.NEGATIVE_INFINITY || result == Double.POSITIVE_INFINITY)
            return null;

        return formatNumber(result);

    }

    /**
     * Format double number.<br>
     * Set '.' (dot) as decimal mark and rounds off number to 4 significant digits
     *
     * @param number any double number
     * @return formatted string containing number
     */
    private String formatNumber(double number) {

        NumberFormat nf = NumberFormat.getNumberInstance(new Locale("en_US"));
        DecimalFormat df = (DecimalFormat) nf;
        df.applyPattern("#.####");

        return df.format(number);

    }

    private static Pattern invalidInfixPattern = Pattern.compile(".*[^\\d\\-+*/().]+.*|.*[.*/+-]{2,}.*");

    /**
     * Convert infix notation of expression to postfix.<br>
     * based on <a href="http://csis.pace.edu/~wolf/CS122/infix-postfix.htm">this algorithm</a>.
     *
     * @param infix expression
     * @return <code>null</code> if there were any errors, otherwise string of postfix expression
     */
    private String infixToPostfix(String infix) {

        if (infix == null)
            return null;

        if (infix.isEmpty())
            return null;

        // check for contains invalid symbols
        if (invalidInfixPattern.matcher(infix).matches())
            return null;

        String[] elements = infix.split("(?<=[-+*/()])|(?=[-+*/()])");

        Map<String, Integer> priority = new HashMap<>();
        priority.put("*", 3);
        priority.put("/", 3);
        priority.put("+", 2);
        priority.put("-", 2);
        priority.put("(", 1);

        StringBuilder postfix = new StringBuilder();

        Deque<String> operatorStack = new ArrayDeque<>();

        try {
            for (String element : elements) {
                try {
                    double number = Double.parseDouble(element);
                    postfix.append(number).append(" ");
                } catch (NumberFormatException e) {
                    switch (element) {
                        case "(":
                            operatorStack.push(element);
                            break;
                        case ")":
                            String topElement = operatorStack.pop();
                            while (!topElement.equals("(")) {
                                postfix.append(topElement).append(" ");
                                topElement = operatorStack.pop();
                            }
                            break;
                        default:
                            while (!operatorStack.isEmpty() &&
                                    priority.get(operatorStack.peek()) >=
                                            priority.get(element))
                                postfix.append(operatorStack.pop()).append(" ");
                            operatorStack.push(element);
                            break;
                    }
                }
            }
        } catch (Exception e) {
            return null;
        }

        while (!operatorStack.isEmpty())
            postfix.append(operatorStack.pop()).append(" ");

        return postfix.toString().trim();

    }

    /**
     * Executes math operation on operands
     *
     * @param operator must be one of them: "*", "/", "-", "*".
     * @param operand1 first operand.
     * @param operand2 second operand.
     * @return double result of calculation
     */
    private double calculate(String operator, double operand1, double operand2) {

        switch (operator) {
            case "*":
                return operand1 * operand2;
            case "/":
                return operand1 / operand2;
            case "+":
                return operand1 + operand2;
            case "-":
                return operand1 - operand2;
        }

        return 0;

    }

}
