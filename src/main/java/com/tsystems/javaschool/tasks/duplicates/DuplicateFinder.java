package com.tsystems.javaschool.tasks.duplicates;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {

        if (!validateArguments(sourceFile, targetFile))
            return false;

        Map<String, Integer> map = new HashMap<>();

        try (Scanner scanner = new Scanner(sourceFile)) {

            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                map.put(line, map.containsKey(line) ? (map.get(line) + 1) : 1);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        printSetToFile(map.entrySet(), targetFile);

        return true;
    }


    /**
     * Checks that arguments are valid
     *
     * @param sourceFile
     * @param targetFile
     * @return <code>false</code> if targetFile is not a file , otherwise
     * <code>true</code>
     */
    private boolean validateArguments(File sourceFile, File targetFile) {

        if (sourceFile == null || targetFile == null)
            throw new IllegalArgumentException();

        return sourceFile.isFile();

    }

    /**
     * Appends lines to file in alphabet order
     *
     * @param linesSet   set contains strings and their number of occurrences
     * @param targetFile output file; append if file exist, create if not.
     */
    private void printSetToFile(Set<Map.Entry<String, Integer>> linesSet, File targetFile) {

        try (PrintWriter writer = new PrintWriter(new FileWriter(targetFile, true))) {
            linesSet.stream().sorted(Comparator.comparing(Map.Entry::getKey))
                    .forEach(e -> writer.printf("%s[%d]%n", e.getKey(), e.getValue()));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
